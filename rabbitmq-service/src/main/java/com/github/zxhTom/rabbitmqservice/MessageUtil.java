package com.github.zxhTom.rabbitmqservice;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.support.converter.ContentTypeDelegatingMessageConverter;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;

import java.util.Map;


/**
 * @author Jinks
 */
public class MessageUtil {
    private static MessageUtil util;

    private MessageUtil(){}

    public static MessageUtil getInstance() {
        if (util == null) {
            util = new MessageUtil();
        }
        return util;
    }

    public Message getMessage(Object msg, Map<String, Object> headMap) {
        MessageConverter converter = new ContentTypeDelegatingMessageConverter(new Jackson2JsonMessageConverter());
        MessageProperties properties = new MessageProperties();
        for (Map.Entry<String, Object> entry : headMap.entrySet()) {
            properties.setHeader(entry.getKey(), entry.getValue());
        }
        return converter.toMessage(msg, properties);
    }
}
