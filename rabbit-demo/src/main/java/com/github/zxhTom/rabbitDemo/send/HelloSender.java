package com.github.zxhTom.rabbitDemo.send;

import com.github.zxhTom.rabbitDemo.model.User;
import com.github.zxhTom.rabbitmqservice.MessageUtil;
import com.github.zxhTom.rabbitmqservice.RabbitConfig;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@Log4j2
public class HelloSender {

    public static Object msg=new User();

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * direction交换机绑定的是queue1(test1)队列,
     * 配置中我们绑定的routeKey是directionKey
     */
    public void directionSend() {
        rabbitTemplate.convertAndSend(RabbitConfig.DIRECTEXCHANGE, "directionKey", msg);
    }

    /**
     * topic zxh.# 绑定 test1
     * zxh   绑定 test2
     */
    public void topicSend1() {
        rabbitTemplate.convertAndSend(RabbitConfig.TOPICEXCHANGE, "zxh", msg);
    }

    public void topicSend2() {
        rabbitTemplate.convertAndSend(RabbitConfig.TOPICEXCHANGE, "zxh.test", msg);
    }

    /**
     * 配置中与headexchange绑定的是test1,test3
     */
    public void headsSend1() {
        Map<String, Object> headMap = new HashMap<String, Object>();
        headMap.put("type", "cash");
        headMap.put("aging", "fast");
        rabbitTemplate.convertAndSend(RabbitConfig.HEADEXCHANGE, RabbitConfig.QUEUEFIRST, MessageUtil.getInstance().getMessage(msg,headMap));
    }

    public void headsSend2() {
        Map<String, Object> headMap = new HashMap<String, Object>();
        headMap.put("aging", "fast");
        rabbitTemplate.convertAndSend(RabbitConfig.HEADEXCHANGE, RabbitConfig.QUEUEFIRST, MessageUtil.getInstance().getMessage(msg,headMap));
    }

    public void headsSend3() {
        Map<String, Object> headMap = new HashMap<String, Object>();
        headMap.put("aging", "fast");
        rabbitTemplate.convertAndSend(RabbitConfig.HEADEXCHANGE, RabbitConfig.QUEUETHIRD, MessageUtil.getInstance().getMessage(msg,headMap));
    }

    public void fanoutSend1() {
        rabbitTemplate.convertAndSend(RabbitConfig.FANOUTEXCHANGE, RabbitConfig.QUEUETHIRD, msg);
    }
    public void fanoutSend2() {
        rabbitTemplate.convertAndSend(RabbitConfig.FANOUTEXCHANGE, RabbitConfig.QUEUETHIRD, msg);
    }

    public void deadSend() {
        MessagePostProcessor processor = new MessagePostProcessor() {
            @Override
            public Message postProcessMessage(Message message) throws AmqpException {
                message.getMessageProperties().setExpiration("60000");
                return message;
            }
        };
        log.info("延时消息开始发送：");
        rabbitTemplate.convertAndSend(RabbitConfig.DIRECTEXCHANGE, RabbitConfig.QUEUEDEAD, msg,processor);
    }
}
