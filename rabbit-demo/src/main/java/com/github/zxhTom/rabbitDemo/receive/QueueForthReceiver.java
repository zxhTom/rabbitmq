package com.github.zxhTom.rabbitDemo.receive;

import com.github.zxhTom.rabbitmqservice.RabbitConfig;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 监听queue4队列的消息并消费
 * @author Jinks
 */
@Component
@RabbitListener(queues = RabbitConfig.QUEUEFORTH)
@Log4j2
public class QueueForthReceiver {
    @RabbitHandler
    public void handler(Object msg){
        log.info(RabbitConfig.QUEUEFORTH+"队列中消费的信息："+msg);
    }
}
