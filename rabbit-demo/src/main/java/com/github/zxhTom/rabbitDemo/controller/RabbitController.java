package com.github.zxhTom.rabbitDemo.controller;

import com.github.zxhTom.rabbitDemo.send.HelloSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rabbit")
public class RabbitController {

    @Autowired
    private HelloSender helloSender1;


    @GetMapping("/direction")
    public void direction() {
        helloSender1.directionSend();
    }

    @GetMapping("/top1")
    public void top1() {
        helloSender1.topicSend1();
    }

    @GetMapping("/head1")
    public void head1() {
        helloSender1.headsSend1();
    }

    @GetMapping("/head2")
    public void head2() {
        helloSender1.headsSend2();
    }
    @GetMapping("/head3")
    public void head3() {
        helloSender1.headsSend3();
    }
    @GetMapping("/top2")
    public void top2() {
        helloSender1.topicSend2();
    }

    @GetMapping("/fanout1")
    public void fanout1() {
        helloSender1.fanoutSend1();
    }
    @GetMapping("/fanout2")
    public void fanout2() {
        helloSender1.fanoutSend2();
    }

    @GetMapping("/dead")
    public void dead() {
        helloSender1.deadSend();
    }
}
