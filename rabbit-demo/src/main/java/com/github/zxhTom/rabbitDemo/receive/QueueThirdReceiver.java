package com.github.zxhTom.rabbitDemo.receive;

import com.github.zxhTom.rabbitDemo.model.User;
import com.github.zxhTom.rabbitmqservice.RabbitConfig;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 监听queue3队列的消息并消费
 * @author Jinks
 */
@Component
@RabbitListener(queues = RabbitConfig.QUEUETHIRD)
@Log4j2
public class QueueThirdReceiver {
    @RabbitHandler
    public void handler(User msg){
        log.info(RabbitConfig.QUEUETHIRD+"队列中消费的信息："+msg);
    }
}
