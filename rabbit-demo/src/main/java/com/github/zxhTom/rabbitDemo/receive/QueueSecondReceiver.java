package com.github.zxhTom.rabbitDemo.receive;

import com.github.zxhTom.rabbitmqservice.RabbitConfig;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 监听queue2队列的消息并消费
 * @author Jinks
 */
@Component
@RabbitListener(queues = RabbitConfig.QUEUESECOND, containerFactory="rabbitListenerContainerFactory")
@Log4j2
public class QueueSecondReceiver {
    @RabbitHandler
    public void handler(Object msg){
        log.info(RabbitConfig.QUEUESECOND+"队列中消费的信息："+msg);
    }
}
