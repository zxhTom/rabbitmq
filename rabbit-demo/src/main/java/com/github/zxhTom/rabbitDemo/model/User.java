package com.github.zxhTom.rabbitDemo.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Jinks
 */
@Data
public class User implements Serializable {
    private int id;
    private String name;
    private String code;
    private boolean sex;
    private Date birthday;

    public User() {
        this.id = 1;
        this.name = "zxh";
        this.code = "zxh";
        this.sex = true;
        this.birthday = new Date();
    }

}
