package com.github.zxhTom.rabbitDemo.receive;

import com.github.zxhTom.rabbitmqservice.RabbitConfig;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class ReceiveCompoent {

    @RabbitListener(queues = "xk.topic.periodSwitch")
    public void receive(Object msg){
        log.info("xk.topic.periodSwitch队列中消费的信息："+msg);
    }
}
